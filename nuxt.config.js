const { CI_PAGES_URL } = process.env
const base = CI_PAGES_URL && new URL(CI_PAGES_URL).pathname

export default {
    target: 'static',
    generate: {
        dir: 'public'
    },
    router: {
        base
    }
}